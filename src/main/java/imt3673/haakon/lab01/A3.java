package imt3673.haakon.lab01;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class A3 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a3);


    }

    public void sendMessage(View view) {
        Intent data = new Intent();

        EditText editText = (EditText) findViewById(R.id.T4);
        String message = editText.getText().toString();

        data.setData(Uri.parse(message));
        setResult(RESULT_OK, data);

        finish();
    }
}
