package imt3673.haakon.lab01;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class A2 extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_a2);

        Intent intent = getIntent();
        String message = intent.getStringExtra(A1.EXTRA_MESSAGE);
        message = "Hello " + message;
        TextView textView = findViewById(R.id.T2);
        TextView textView2 = findViewById(R.id.T3);
        textView.setText(message);
        textView2.setText("From A3: ");
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                String message = data.getDataString();
                TextView textView2 = findViewById(R.id.T3);
                textView2.setText("From A3: " + message);
            }
        }
    }

    public void sendMessage(View view) {
        Intent intent = new Intent(this, A3.class);

        startActivityForResult(intent, 1);
    }
}
